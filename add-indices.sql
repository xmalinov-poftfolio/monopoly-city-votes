ALTER TABLE `cities` ADD INDEX(`id`);
ALTER TABLE `cities` ADD INDEX(`name`);
ALTER TABLE `cities` ADD INDEX(`skip`);

ALTER TABLE `coords` ADD INDEX(`id`);
ALTER TABLE `coords` ADD INDEX(`place_id`);

ALTER TABLE `votes` ADD INDEX(`id`);
ALTER TABLE `votes` ADD INDEX(`city`);
ALTER TABLE `votes` ADD INDEX(`canceled`);
