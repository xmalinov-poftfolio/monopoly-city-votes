<?php
error_reporting(0);
include('dbconf.php');

$postError = 'error';
$postSuccess = 'success';

//vote accepted - 280
//already voted - 288
//city adding error - 480
//vote adding error - 481
//data in post isn't received or wrong - 488
//could not connect - 588

function postResponse($status, $code)
{
    header("Content-type: application/json; charset=utf-8");
    $returnData = array('status' => $status, 'code' => $code);
    echo json_encode($returnData);
}

function addUser($id, $city, $fname, $lname, $bdate, $socialtype, $socialurl)
{
    $s = $GLOBALS['serverName'];
    $u = $GLOBALS['userName'];
    $p = $GLOBALS['password'];
    $d = $GLOBALS['dbName'];

    $dt = date("Y-m-d H:i:s");

    $link = mysqli_connect($s, $u, $p, $d);

    //фикс кодировки кириллицы
    mysqli_query($link, "set_client='utf8'");
    mysqli_query($link, "set character_set_results='utf8'");
    mysqli_query($link, "set collation_connection='utf8_general_ci'");
    mysqli_query($link, "SET NAMES utf8");

    if ($link === false) {
        postResponse($GLOBALS['postError'], '588'); //не соединились с базой
        die();
    }

    //проверка на наличие города в бд, если нет, то добавить город
    $checkQuery = "SELECT * FROM `users` WHERE socialid = '$id'";
    $checkQueryResult = mysqli_query($link, $checkQuery);

    if (mysqli_num_rows($checkQueryResult) === 0) {
        $userQuery = "INSERT INTO `users` (`socialid`, `city`,`fname`, `lname`, `bdate`, `socialtype`, `socialurl`,  `added`) VALUES ('$id','$city', '$fname', '$lname', '$bdate', '$socialtype', '$socialurl', '$dt')";

        if (mysqli_query($link, $userQuery) !== true) {
            postResponse($GLOBALS['postError'], '418');
        }

        postResponse($GLOBALS['postSuccess'], '280');
    } else {
        postResponse($GLOBALS['postSuccess'], '288');
    }

    mysqli_free_result($checkQueryResult);
    mysqli_close($link);
}

if (isset($_POST['id']) & isset($_POST['city']) & isset($_POST['fname']) & isset($_POST['lname']) & isset($_POST['bdate']) & isset($_POST['socialtype']) & isset($_POST['socialurl'])) {

    $id = $_POST['socialtype'] . $_POST['id'];
    $city = $_POST['city'];
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $bdate = $_POST['bdate'];
    $socialtype = $_POST['socialtype'];
    $socialurl = $_POST['socialurl'];

    addUser($id, $city, $fname, $lname, $bdate, $socialtype, $socialurl);
} else {
    postResponse($postError, '488'); //проблемы с параметрами post
}