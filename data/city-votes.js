//http://geojson.org/
eqfeed_callback({
        "type": "FeatureCollection",
        "metadata": {
            "generated": 1395197681000,
            "url": "//",
            "title": "City votes",
            "status": 200,
            "count": 1021
        },
        "features": [{
            "type": "Feature",
            "properties": {
                "votes": 100,
                "city": "Yaroslavl",
                "time": 1395196367200,
                "updated": 1395197427537
            },
            "geometry": {
                "type": "Point",
                "coordinates": [37.6153054, 55.7520263]
            },
            "id": "1"
        }, {
            "type": "Feature",
            "properties": {
                "votes": 420,
                "city": "Moscow",
                "time": 1395196367200,
                "updated": 1395197427537
            },
            "geometry": {
                "type": "Point",
                "coordinates": [39.878677, 57.6012389]
            },
            "id": "2"
        }]
    }
);