




var currentCity = {
    'name': '',
    'allNames': []
};

//social auth checker
var currentUser = {
    'mode': 'demo',
    'isFilled': false
};

function setCurrentUser(id, city, fname, lname) {
    $('#auth-debug').html(id + '; ' + fname + ' ' + lname + '; ' + city);

    currentUser.id = id;
    currentUser.city = city;
    currentUser.fname = fname;
    currentUser.lname = lname;
    currentUser.isFilled = true;
}

function cityArray(list) {
    var cityArray = [];

    $.each(list, function (index, value) {
        cityArray.push($(value).text());
    });

    return cityArray;
}

function setCurrentCityList() {
    var cityListTop = $('.city-list').position().top;
    var cityListItems = $('.city-list li');

    var currItemNum = Math.abs((cityListTop - 120) / 32);

    currentCity.name = currentCity.allNames[currItemNum];

    cityListItems.removeAttr('contenteditable');
    $(cityListItems[currItemNum]).attr('contenteditable', 'true');
    console.log(currentCity.name, $(cityListItems[currItemNum]));

    $('li[contenteditable="true"]').on('focus', function (e) {
        //currentCity.name = $(this).text();
        $(this).text('');
    });

    $('li[contenteditable="true"]').on('focusout', function (e) {
        $(this).text(currentCity.name);
    });


    $('li[contenteditable="true"]').on('input propertychange paste', function (e) {
        //inputController(e, this);
        //console.log(e);
    });
}

function moveListToCurrentCity(item) {
    //console.log(item.index());
    var newHeight = 120 - (item.index() * 32);

    $('.city-list').css({
        'top': newHeight + 'px'
    });

    setCurrentCityList();
}

function inputController(e, listItem) {
    e.preventDefault();
    var currentText = $(listItem).text();
    console.info(currentCity.name);
    //$(listItem).text('');
}

function writeUserInfo(id, city, fname, lname, bdate, social) {
    var socialType = social || '';
    var socialUrl = '';

    if (social === 'vk') {
        socialUrl = 'https://vk.com/id' + id;
    }

    if (social === 'ok') {
        socialUrl = 'https://ok.ru/profile/' + id;
    }

    //TODO сделать корректный паресер дат рождения
    var bDate = new Date(bdate);

    var dataString = {
        'id': id,
        'city': city,
        'fname': fname,
        'lname': lname,
        'bdate': bDate.toMysqlFormat(),
        'socialtype': socialType,
        'socialurl': socialUrl
    };

    //console.info(dataString);

    $.ajax({
        type: 'POST',
        url: 'user.php',
        data: dataString,
        success: function (data) {
            //console.log(data);
            if (data.code == 288) {
                console.log('Пользователь уже есть в базе');
            }
            if (data.code == 280) {
                console.log('Пользователь добавлен');
            }
        }
    });
}

$(document).ready(function () {
    if (window.location.href.indexOf('vk.com') > -1) {
        runVK();
    } else if (window.location.href.indexOf('ok.ru') > -1) {
        runOK();
    } else {
        runDemo();
    }

    getCities();
    refreshTop();
});

function runDemo() {
    console.info('Demo mode');

    currentUser.mode = 'demo';
}

function runVK() {
    console.info('VK mode');

    currentUser.mode = 'vk';

    VK.init(function () {
        console.info('inited');

        VK.api('users.get', {
            'fields': 'sex, bdate, city, country, photo_50, photo_100,verified'
        }, function (data) {
            if (data.response) {
                // data.response is object
                //
                //console.info(data.response[0]);
                var r = data.response[0];

                setCurrentUser(r.id, r.city.title, r.first_name, r.last_name);
                writeUserInfo(r.id, r.city.title, r.first_name, r.last_name, r.bdate, 'vk');
            }
        });
    }, function () {
        console.info('init failed');
    }, '5.44');
}

function runOK() {
    console.info('OK mode');

    currentUser.mode = 'ok';

    var rParams = FAPI.Util.getRequestParameters();
    FAPI.init(rParams["api_server"], rParams["apiconnection"],
        function () {
            console.info("Инициализация прошла успешно");

            FAPI.Client.call({
                'method': 'users.getCurrentUser',
                'fields': 'uid, first_name, last_name, name, gender, age, birthday, photo_id, pic_1, pic_2, location'
            }, function (method, result, data) {
                if (result) {
                    console.info(result);
                    var r = result;

                    setCurrentUser(r.uid, r.location.city, r.first_name, r.last_name);
                    writeUserInfo(r.uid, r.location.city, r.first_name, r.last_name, r.birthday, 'ok');
                } else {
                    console.log(data);
                }
            });
        },
        function (error) {
            console.info('Ошибка инициализации', error);
        }
    );
}

function getCities() {
    $.ajax({
        type: 'POST',
        url: 'getcities.php',
        data: {},
        success: function (data) {
            var buffer = '';

            $.each(data, function (item, value) {
                buffer += '<li class="city">' + value.name + '</li>';
            });

            $('ul.city-list').html(buffer);


            //list controller
            var itemHeight = $('.city-list li').height();

            var inputHeight = $('.city-input').height();
            var inputTop = $('.city-input').position().top;

            var cityList = $('.city-list');
            var cityListLength = $('.city-list li').length;

            currentCity.allNames = cityArray($('.city-list li'));

            //console.log(cityListLength);

            function nextItem() {
                var cityListTop = cityList.position().top;

                if (cityListTop <= (inputTop - itemHeight * (cityListLength - 1))) {
                    return;
                }

                cityList.css({
                    'top': cityListTop - itemHeight + 'px'
                });

                setCurrentCityList();
            }

            function prevItem() {
                var cityListTop = cityList.position().top;

                if (cityListTop >= inputTop) {
                    return;
                }

                cityList.css({
                    'top': cityListTop + itemHeight + 'px'
                });

                setCurrentCityList();
            }


            $(window).keydown(function (e) {
                if (e.which === 40) {
                    e.preventDefault();
                    //console.log('down');
                    nextItem();
                } else if (e.which === 38) {
                    e.preventDefault();
                    //console.log('up');
                    prevItem();
                }
            });

            var mousewheelEvent = (/Firefox/i.test(navigator.userAgent)) ? 'DOMMouseScroll' : 'mousewheel'; //FF doesn't recognize mousewheel as of FF3.x
            $('.list-container').bind(mousewheelEvent, function (e) {

                e.preventDefault();

                var event = window.event || e; //equalize event object
                event = event.originalEvent ? event.originalEvent : event; //convert to originalEvent if possible
                var delta = event.detail ? event.detail * (-40) : event.wheelDelta; //check for detail first, because it is used by Opera and FF

                var direction = '';
                if (delta > 0) {
                    //console.log(delta);
                    direction = 'up';
                } else {
                    //console.log(delta);
                    direction = 'down';
                }

                if (direction === 'up') {
                    prevItem();
                }
                if (direction === 'down') {
                    nextItem();
                }

                clearTimeout($.data(this, 'scrollTimer'));
                $.data(this, 'scrollTimer', setTimeout(function () {
                    // do something

                    //console.log("Haven't scrolled in 250ms!");
                }, 20));
            });

            $('.city').on('click', function () {
                //console.log($(this).text());

                moveListToCurrentCity($(this));

                //var container = $(document.getElementById('form_with_map'));
                //var cityInput = container.find('[name="city"]');
                //var selectedCity = $(this).text();

                //cityInput.val(selectedCity);
                //currentCity.name = selectedCity;
            });

            //setCurrentCityList();

            //console.log(data);

            var options = {
                data: data,
                getValue: 'name',
                list: {
                    match: {
                        enabled: true
                    }
                }
            };

            $('#city-input').easyAutocomplete(options);
        }
    });
}

function getVotes(limit) {
    var dataString = {
        'toplimit': limit
    };

    $.ajax({
        type: 'POST',
        url: 'getvotes.php',
        data: dataString,
        success: function (data) {
            //console.log(data);
            var buffer = '';

            $.each(data, function (item, value) {
                buffer += '<li class="top"><span class="votes-name">' + value.name + '</span> - <span class="votes-count">' + value.count + '</span></li>';
            });

            $('.city-top' + limit + ' ol').html(buffer);

            $('.city-top' + limit + ' .top').on('click', function () {
                console.log($(this).find('.votes-name').text());

                var container = $(document.getElementById('form_with_map'));

                var cityInput = container.find('[name="city"]');

                var selectedCity = $(this).find('.votes-name').text();

                cityInput.val(selectedCity);

                currentCity.name = selectedCity;
            });
        }
    });
}

function getRating(limit) {
    var dataString = {
        'toplimit': limit
    };

    $.ajax({
        type: 'POST',
        url: 'gettotalvotes.php',
        data: {},
        success: function (data) {
            var totalVotes = data[0].count;

            $.ajax({
                type: 'POST',
                url: 'getvotes.php',
                data: dataString,
                success: function (data) {
                    var buffer = '';

                    $.each(data, function (item, value) {
                        var currIndex = item + 1;
                        var percents = Math.round(parseFloat(value.count / totalVotes * 100) * 100) / 100;
                        var stripeWidth = Math.round(240 * percents / 100);

                        var numbersLeft = (percents < 10) ? 38 : stripeWidth + 8;

                        if (percents < 40) {
                            buffer += '<li><div class="top20-city">' + value.name + '</div><div class="top20-info"><div class="top-stripe listbg' + currIndex + '" style="width:' + stripeWidth + 'px;"><div class="top20-numbers" style="left:' + numbersLeft + 'px;"><span class="top-percent">' + percents + '%</span> / <span class="top-total">' + value.count + '</span><span class="plus-one"> +1</span></div></div></div><div class="vote-btn">Голосовать</div></li>';
                        } else {
                            buffer += '<li><div class="top20-city">' + value.name + '</div><div class="top20-info"><div class="top-stripe listbg' + currIndex + '" style="width:' + stripeWidth + 'px;"><div class="top20-numbers"><span class="top-percent">' + percents + '%</span> / <span class="top-total">' + value.count + '</span><span class="plus-one"> +1</span></div></div></div><div class="vote-btn">Голосовать</div></li>';
                        }
                    });

                    $('ul.top20-list').html(buffer);

                    $('.vote-btn').click(function () {
                        console.log($($(this).siblings()[0]).text());
                        currentCity.name = $($(this).siblings()[0]).text();

                        voteForCity();
                    });

                    //$('.city-top' + limit + ' .top').on('click', function () {
                    //    console.log($(this).find('.votes-name').text());
                    //
                    //    var container = $(document.getElementById('form_with_map'));
                    //
                    //    var cityInput = container.find('[name="city"]');
                    //
                    //    var selectedCity = $(this).find('.votes-name').text();
                    //
                    //    cityInput.val(selectedCity);
                    //
                    //    currentCity.name = selectedCity;
                    //});
                }
            });
        }
    });
}

function refreshTop() {
    getVotes(10);
    getVotes(20);

    getRating(20);
}

