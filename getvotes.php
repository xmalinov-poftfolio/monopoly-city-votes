<?php
error_reporting(0);
include('dbconf.php');

$postError = 'error';
$postSuccess = 'success';

function postResponse($status, $code)
{
    header("Content-type: application/json; charset=utf-8");
    $returnData = array('status' => $status, 'code' => $code);
    echo json_encode($returnData);
}

$link = mysqli_connect($serverName, $userName, $password, $dbName);

//фикс кодировки кириллицы
mysqli_query($link, "set_client='utf8'");
mysqli_query($link, "set character_set_results='utf8'");
mysqli_query($link, "set collation_connection='utf8_general_ci'");
mysqli_query($link, "SET NAMES utf8");

if ($link === false) {
    postResponse($GLOBALS['postError'], '588'); //не соединились с базой
    die();
}

if (isset($_POST['toplimit'])) {
    $topLimit = $_POST['toplimit'];

    if (empty($topLimit)) {
        $resultQuery = "CALL getTop()";
    } else {
        $resultQuery = "CALL getTop20()";
    }

    $resultQueryResult = mysqli_query($link, $resultQuery);

    $rows = array();
    while ($r = mysqli_fetch_assoc($resultQueryResult)) {
        $rows[] = $r;
    }

    header("Content-type: application/json; charset=utf-8");
    echo json_encode($rows, JSON_UNESCAPED_UNICODE);

    mysqli_close($link);

} else {
    postResponse($postError, '488'); //проблемы с параметрами post
}