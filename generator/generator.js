var counter = 0;
var isTimeOut = false;
var isRandomMode = false;

$(document).ready(function () {
    getCities();

    $('#random-vote').on('change', lockSelector);

    $('#generate-votes').on('click', generateVotes);
});

function getCities() {
    $.ajax({
        type: 'POST',
        url: '../getcities.php',
        data: {},
        success: function (data) {
            var buffer = '';

            $.each(data, function (item, value) {
                buffer += '<option>' + value.name + '</option>';
            });

            $('#city-selector').html(buffer);
        }
    });
}

function lockSelector() {
    if ($(this).is(":checked")) {
        $('#city-selector').prop('disabled', 'disabled');
        isRandomMode = true;
    }
    else {
        $('#city-selector').prop('disabled', false);
    }
}

function generateVotes() {
    var timerSeconds = $('#timer').val();
    $('#vote-counter').text('');

    counter = 0;
    isTimeOut = false;

    if (timerSeconds > 0) {
        vote();
        setTimeout(function () {
            isTimeOut = true;
            $('#vote-counter').text(counter);
        }, timerSeconds * 1000);
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function vote() {
    var selectedCity = '';

    if (isRandomMode) {
        var max = $('option').length;
        selectedCity = $($('option')[getRandomInt(0, max)]).text();
    } else {
        selectedCity = $('#city-selector').val();
    }

    var dataString = {
        'city': selectedCity,
        'socialID': 'votes-generator'
    };

    $.ajax({
        type: "POST",
        url: "../vote.php",
        data: dataString,
        success: function (data) {
            if (data.code == 288) {
                //revote
            }
            if (data.code == 280) {
                //done
                counter++;
                if (!isTimeOut) {
                    vote();
                }
            }
            if (data.code == 480) {
                //error
            }
        }
    });
}