<?php
include('dbconf.php');

$postError = 'error';
$postSuccess = 'success';


function postResponse($status, $code)
{
    header("Content-type: application/json; charset=utf-8");
    $returnData = array('status' => $status, 'code' => $code);
    echo json_encode($returnData);
}

$link = mysqli_connect($serverName, $userName, $password, $dbName);

//фикс кодировки кириллицы
mysqli_query($link, "set_client='utf8'");
mysqli_query($link, "set character_set_results='utf8'");
mysqli_query($link, "set collation_connection='utf8_general_ci'");
mysqli_query($link, "SET NAMES utf8");

if ($link === false) {
    postResponse($GLOBALS['postError'], '588');
    die();
}

//https://geocode-maps.yandex.ru/1.x/?geocode=Москва


//$xml = new SimpleXMLElement($xmlString);
//echo $xml->bbb->cccc->dddd['Id'];
//echo $xml->bbb->cccc->eeee['name'];
//// or...........
//foreach ($xml->bbb->cccc as $element) {
//    foreach($element as $key => $val) {
//        echo "{$key}: {$val}";
//    }
//}

$citiesQuery = "SELECT name FROM `cities`";
$citiesQueryResult = mysqli_query($link, $citiesQuery);


$params = array(
    'geocode' => 'Москва', // адрес
    'format' => 'json',
    'results' => 1
);


$response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));

if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0) {
    echo $response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos;
} else {
    echo 'Ничего не найдено';
}


mysqli_close($link);