-- phpMyAdmin SQL Dump
-- version 4.4.15.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 15, 2016 at 01:42 PM
-- Server version: 5.5.47
-- PHP Version: 5.6.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `monopoly-votes-prod`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(24) DEFAULT NULL,
  `skip` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `skip`) VALUES
(1, 'Абакан', 0),
(2, 'Анадырь', 0),
(3, 'Архангельск', 0),
(4, 'Астрахань', 0),
(5, 'Барнаул', 0),
(6, 'Белгород', 0),
(7, 'Биробиджан', 0),
(8, 'Благовещенск', 0),
(9, 'Брянск', 0),
(10, 'Великий Новгород', 0),
(11, 'Владивосток', 0),
(12, 'Владикавказ', 0),
(13, 'Владимир', 0),
(14, 'Волгоград', 0),
(15, 'Вологда', 0),
(16, 'Воронеж', 0),
(17, 'Горно-Алтайск', 0),
(18, 'Грозный', 0),
(19, 'Екатеринбург', 0),
(20, 'Иваново', 0),
(21, 'Ижевск', 0),
(22, 'Иркутск', 0),
(23, 'Йошкар-Ола', 0),
(24, 'Казань', 0),
(25, 'Калининград', 0),
(26, 'Калуга', 0),
(27, 'Кемерово', 0),
(28, 'Киров', 0),
(29, 'Кострома', 0),
(30, 'Краснодар', 0),
(31, 'Красноярск', 0),
(32, 'Курган', 0),
(33, 'Курск', 0),
(34, 'Кызыл', 0),
(35, 'Липецк', 0),
(36, 'Магадан', 0),
(37, 'Магас', 0),
(38, 'Майкоп', 0),
(39, 'Махачкала', 0),
(40, 'Москва', 0),
(41, 'Мурманск', 0),
(42, 'Нальчик', 0),
(43, 'Нарьян-Мар', 0),
(44, 'Нижний Новгород', 0),
(45, 'Новосибирск', 0),
(46, 'Омск', 0),
(47, 'Оренбург', 0),
(48, 'Орёл', 0),
(49, 'Пенза', 0),
(50, 'Пермь', 0),
(51, 'Петрозаводск', 0),
(52, 'Петропавловск-Камчатский', 0),
(53, 'Псков', 0),
(54, 'Ростов-на-Дону', 0),
(55, 'Рязань', 0),
(56, 'Салехард', 0),
(57, 'Самара', 0),
(58, 'Санкт-Петербург', 0),
(59, 'Саранск', 0),
(60, 'Саратов', 0),
(61, 'Смоленск', 0),
(62, 'Ставрополь', 0),
(63, 'Сыктывкар', 0),
(64, 'Тамбов', 0),
(65, 'Тверь', 0),
(66, 'Томск', 0),
(67, 'Тула', 0),
(68, 'Тюмень', 0),
(69, 'Улан-Удэ', 0),
(70, 'Ульяновск', 0),
(71, 'Уфа', 0),
(72, 'Хабаровск', 0),
(73, 'Ханты-Мансийск', 0),
(74, 'Чебоксары', 0),
(75, 'Челябинск', 0),
(76, 'Черкесск', 0),
(77, 'Чита', 0),
(78, 'Элиста', 0),
(79, 'Южно-Сахалинск', 0),
(80, 'Якутск', 0),
(81, 'Ярославль', 0);

-- --------------------------------------------------------

--
-- Table structure for table `coords`
--

CREATE TABLE IF NOT EXISTS `coords` (
  `id` int(11) NOT NULL,
  `place_id` varchar(8) DEFAULT NULL,
  `latitude` varchar(8) DEFAULT NULL,
  `longitude` varchar(9) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coords`
--

INSERT INTO `coords` (`id`, `place_id`, `latitude`, `longitude`) VALUES
(1, '1', '53.72235', '91.443699'),
(2, '2', '64.73143', '177.50152'),
(3, '3', '64.53939', '40.516939'),
(4, '4', '46.34786', '48.033574'),
(5, '5', '53.34805', '83.779875'),
(6, '6', '50.59746', '36.588849'),
(7, '7', '48.79466', '132.92173'),
(8, '8', '50.29065', '127.52717'),
(9, '9', '53.24332', '34.363731'),
(10, '10', '58.52147', '31.275475'),
(11, '11', '43.11641', '131.88247'),
(12, '12', '43.02060', '44.681888'),
(13, '13', '56.12904', '40.40703'),
(14, '14', '48.70710', '44.516939'),
(15, '15', '59.22049', '39.891568'),
(16, '16', '51.66153', '39.200287'),
(17, '17', '51.95818', '85.960373'),
(18, '18', '43.31777', '45.694909'),
(19, '19', '56.83860', '60.605514'),
(20, '20', '57.00034', '40.973921'),
(21, '21', '56.85277', '53.211463'),
(22, '22', '52.28638', '104.28066'),
(23, '23', '56.63440', '47.899878'),
(24, '24', '55.79855', '49.106324'),
(25, '25', '54.70739', '20.507307'),
(26, '26', '54.50701', '36.252277'),
(27, '27', '55.35496', '86.087314'),
(28, '28', '58.60358', '49.668023'),
(29, '29', '57.76796', '40.926858'),
(30, '30', '45.04021', '38.975996'),
(31, '31', '56.01056', '92.852545'),
(32, '32', '55.44160', '65.344316'),
(33, '33', '51.73036', '36.192647'),
(34, '34', '51.71908', '94.437757'),
(35, '35', '52.61022', '39.594719'),
(36, '36', '59.56816', '150.80854'),
(37, '37', '43.16877', '44.813087'),
(38, '38', '44.60976', '40.100516'),
(39, '39', '42.98306', '47.504682'),
(40, '40', '55.75396', '37.620393'),
(41, '41', '68.96956', '33.07454'),
(42, '42', '43.48527', '43.607072'),
(43, '43', '67.63806', '53.006872'),
(44, '44', '56.32688', '44.005986'),
(45, '45', '55.03019', '82.92043'),
(46, '46', '54.98934', '73.368212'),
(47, '47', '51.76819', '55.096955'),
(48, '48', '52.96718', '36.069613'),
(49, '49', '53.19454', '45.019529'),
(50, '50', '58.01025', '56.234195'),
(51, '51', '61.78903', '34.359688'),
(52, '52', '53.03699', '158.65595'),
(53, '53', '57.81936', '28.331786'),
(54, '54', '47.22253', '39.718705'),
(55, '55', '54.62916', '39.734928'),
(56, '56', '66.53071', '66.613851'),
(57, '57', '53.19552', '50.101819'),
(58, '58', '59.93909', '30.315868'),
(59, '59', '54.18085', '45.186319'),
(60, '60', '51.53310', '46.034158'),
(61, '61', '54.78252', '32.044128'),
(62, '62', '45.04450', '41.969065'),
(63, '63', '61.66867', '50.835716'),
(64, '64', '52.72124', '41.452238'),
(65, '65', '56.85961', '35.911896'),
(66, '66', '56.48468', '84.948197'),
(67, '67', '54.19303', '37.617752'),
(68, '68', '57.15303', '65.534328'),
(69, '69', '51.83350', '107.58412'),
(70, '70', '54.31241', '48.395622'),
(71, '71', '54.73477', '55.957829'),
(72, '72', '48.47258', '135.05773'),
(73, '73', '61.00248', '69.018408'),
(74, '74', '56.13865', '47.239894'),
(75, '75', '55.16028', '61.400856'),
(76, '76', '44.22686', '42.04677'),
(77, '77', '52.03397', '113.49943'),
(78, '78', '46.30830', '44.270181'),
(79, '79', '46.95917', '142.73804'),
(80, '80', '62.02809', '129.73255'),
(81, '81', '57.62654', '39.893885');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `socialid` varchar(256) NOT NULL,
  `fname` varchar(256) NOT NULL,
  `lname` varchar(256) NOT NULL,
  `city` varchar(256) NOT NULL,
  `socialurl` varchar(256) NOT NULL,
  `socialtype` varchar(256) NOT NULL,
  `bdate` date NOT NULL,
  `added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `id` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `votedate` datetime NOT NULL,
  `socialid` varchar(256) NOT NULL,
  `canceled` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `coords`
--
ALTER TABLE `coords`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `votes`
--
ALTER TABLE `votes`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `coords`
--
ALTER TABLE `coords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `votes`
--
ALTER TABLE `votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
