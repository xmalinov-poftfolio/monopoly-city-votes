<?php
error_reporting(0);
include('dbconf.php');

$postError = 'error';
$postSuccess = 'success';

function postResponse($status, $code)
{

    $returnData = array('status' => $status, 'code' => $code);
    echo json_encode($returnData);
}

$link = mysqli_connect($serverName, $userName, $password, $dbName);

//фикс кодировки кириллицы
mysqli_query($link, "set_client='utf8'");
mysqli_query($link, "set character_set_results='utf8'");
mysqli_query($link, "set collation_connection='utf8_general_ci'");
mysqli_query($link, "SET NAMES utf8");

if ($link === false) {
    postResponse($GLOBALS['postError'], '588'); //не соединились с базой
    die();
}

$citiesQuery = "SELECT name FROM `cities` WHERE `skip` = 0 ORDER BY name";
$citiesQueryResult = mysqli_query($link, $citiesQuery);

$rows = array();
while($r = mysqli_fetch_assoc($citiesQueryResult)) {
    $rows[] = $r;
}

header("Content-type: application/json; charset=utf-8");
echo json_encode($rows, JSON_UNESCAPED_UNICODE);

mysqli_close($link);