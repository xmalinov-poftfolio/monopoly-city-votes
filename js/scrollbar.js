var scrollbar = $('#scroll-bar-l');
var scrollbarBlock = $('#scroll-bar-b');

function setScrollbarPos() {
  var cityListTop = $('.city-list').position().top;
  var cityListItems = $('.city-list li').length;

  var currItemNum = Math.abs((cityListTop - 120) / 32);

  var range = scrollbar.height() - scrollbarBlock.height();
  var scrollbarBlockTop = Math.round(currItemNum / cityListItems * range);

  scrollbarBlock.css({'top': scrollbarBlockTop});
}