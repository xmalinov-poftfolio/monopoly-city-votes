var map, heatmap;

var loadCounter = 0;

function initMap() {
  map = new google.maps.Map(document.getElementById('google-map'), {
    zoom: 4,
    //center: {lat: 55.742475, lng: 37.480966},
    center: {lat: 63.0700981, lng: 49.8045973},
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  $.ajax({
    type: 'POST',
    url: 'getgeojson.php',
    data: {},
    xhr: function () {
      var xhr = new XMLHttpRequest();
      //Download progress
      xhr.addEventListener("progress", function (evt) {
        loadCounter++;
        $('.map-preloader-nums span').text(Math.round(loadCounter / 10));
        var currL = -515 + (415 * loadCounter / 1000);
        $('.map-preloader-back').show();
        $('.map-preloader-back').css('left', currL + 'px');
        if (evt.lengthComputable) {
          var percentComplete = evt.loaded / evt.total;
          //Do something with download progress
          console.log(percentComplete);
        }
      }, false);
      return xhr;
    },
    success: function (data) {
      //console.log(data.features);
      var layoffLocs = [];

      $.each(data, function (i, value) {
        layoffLocs.push(new google.maps.LatLng(value.lat, value.long));
      });

      var pointArray = new google.maps.MVCArray(layoffLocs);
      var heatmap = new google.maps.visualization.HeatmapLayer({
        data: pointArray,
        gradient: [
          'rgba(113, 85, 184, 0)',
          'rgba(127, 85, 184, 1)',
          'rgba(141, 85, 184, 1)',
          'rgba(155, 85, 184, 1)',
          'rgba(170, 85, 184, 1)',
          'rgba(184, 85, 184, 1)',
          'rgba(198, 85, 184, 1)',
          'rgba(212, 85, 170, 1)',
          'rgba(255, 99, 170, 1)'
        ],
        radius: 40
      });

      console.log('loaded');
      heatmap.setMap(map);

      //heatmap.set('radius', 40);

      var cc = setInterval(function () {
        loadCounter++;
        $('.map-preloader-nums span').text(loadCounter);

        var currL = -515 + (415 * loadCounter / 100);
        $('.map-preloader-back').show();
        $('.map-preloader-back').css('left', currL + 'px');

        if (loadCounter > 100) {
          $('#map-preloader').hide();
          clearInterval(cc);
        }
      }, 50);
    }
  });
}

function toggleHeatmap() {
  heatmap.setMap(heatmap.getMap() ? null : map);
}

function changeGradient() {
  //var gradient = [
  //    'rgba(0, 255, 255, 0)',
  //    'rgba(0, 255, 255, 1)',
  //    'rgba(0, 191, 255, 1)',
  //    'rgba(0, 127, 255, 1)',
  //    'rgba(0, 63, 255, 1)',
  //    'rgba(0, 0, 255, 1)',
  //    'rgba(0, 0, 223, 1)',
  //    'rgba(0, 0, 191, 1)',
  //    'rgba(0, 0, 159, 1)',
  //    'rgba(0, 0, 127, 1)',
  //    'rgba(63, 0, 91, 1)',
  //    'rgba(127, 0, 63, 1)',
  //    'rgba(191, 0, 31, 1)',
  //    'rgba(255, 0, 0, 1)'
  //];
  var gradient = [
    'rgba(255, 99, 170, 0)',
    'rgba(212, 85, 170, 1)',
    'rgba(198, 85, 184, 1)',
    'rgba(184, 85, 184, 1)',
    'rgba(170, 85, 184, 1)',
    'rgba(155, 85, 184, 1)',
    'rgba(141, 85, 184, 1)',
    'rgba(127, 85, 184, 1)',
    'rgba(113, 85, 184, 1)',
    'rgba(99, 85, 184, 1)',
    'rgba(85, 85, 198, 1)',
    'rgba(70, 85, 198, 1)',
    'rgba(56, 70, 198, 1)',
    'rgba(56, 85, 198, 1)',
    'rgba(42, 70, 198, 1)'
  ];
  //var gradient = [
  //    'rgba(255, 0, 0, 0)',
  //    'rgba(255, 255, 0, 0.9)',
  //    'rgba(0, 255, 0, 0.7)',
  //    'rgba(173, 255, 47, 0.5)',
  //    'rgba(152, 251, 152, 0)',
  //    'rgba(152, 251, 152, 0)',
  //    'rgba(0, 0, 238, 0.5)',
  //    'rgba(186, 85, 211, 0.7)',
  //    'rgba(255, 0, 255, 0.9)',
  //    'rgba(255, 0, 0, 1)'];

  heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
}

function changeRadius() {
  heatmap.set('radius', heatmap.get('radius') ? null : 20);
}

function changeOpacity() {
  heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
}
