$('.footer-big-fb').on('click', function () {
  shareResult('fb');
});

$('.footer-big-vk').on('click', function () {
  shareResult('vk');
});

$('.footer-big-ok').on('click', function () {
  shareResult('ok');
});

//function shareResult(socialType) {
//    switch (socialType) {
//        case 'vk':
//            openPopupCenter('//vk.com/share.php?url=https://monopoly.hasbrogames.ru&title=Я проголосовал за город «' + currentCity.name + ' »&image=https://monopoly.hasbrogames.ru/img/share.jpg&description=Решать тебе, какие города появятся на доске новой игры «Монополия Россия»!', 'Голосуй за любимый город', 655, 423);
//            break;
//        case 'ok':
//            openPopupCenter('//www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=https://monopoly.hasbrogames.ru&st.comments=' + encodeURIComponent('Решать тебе, какие города появятся на доске новой игры «Монополия Россия»!'), 'Голосуй за любимый город', 580, 336);
//            break;
//        default:
//            //
//            break;
//    }
//}

var shareURL = '';
var shareTitle = '';
var shareDescription = '';

function getShareMeta() {
  var metas = document.getElementsByTagName('meta');

  for (var i = 0; i < metas.length; i++) {
    if (metas[i].getAttribute('property') == 'og:url') {
      shareURL = metas[i].getAttribute('content');
    }
    if (metas[i].getAttribute('property') == 'og:title') {
      shareTitle = metas[i].getAttribute('content');
    }
    if (metas[i].getAttribute('property') == 'og:description') {
      shareDescription = metas[i].getAttribute('content');
    }
  }
}

function openPopupCenter(url, title, w, h) {
  var wLeft = window.screenLeft ? window.screenLeft : window.screenX;
  var wTop = window.screenTop ? window.screenTop : window.screenY;

  var left = wLeft + (window.innerWidth / 2) - (w / 2);
  var top = wTop + (window.innerHeight / 2) - (h / 2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

function shareResult(socialType) {
  getShareMeta();
  switch (socialType) {
    case 'fb':
      openPopupCenter('//www.facebook.com/share.php?u=' + shareURL, shareTitle, 655, 327);
      break;
    case 'vk':
      openPopupCenter('//vk.com/share.php?url=' + shareURL + '&description=' + shareDescription, shareTitle, 655, 423);
      break;
    case 'ok':
      openPopupCenter('//www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=' + shareURL + '&st.comments=' + shareDescription, shareTitle, 580, 336);
      break;
    default:
      //
      break;
  }
}