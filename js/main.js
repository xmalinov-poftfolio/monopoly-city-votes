var currentCity = {
  'name': '',
  'allNames': []
};

var currentUser = {
  'mode': 'site',
  'isFilled': false
};

var canVote = false;
var isInputMode = false;

$(document).ready(function () {
  if (window.location.href.indexOf('vk.com') > -1) {
    runVK();
  } else if (window.location.href.indexOf('ok.ru') > -1) {
    runOK();
  } else {
    runSite();
  }

  $('#vote-btn-top').css('opacity', '0.6');

  getCities();
  refreshTop();
});

function voteForCity() {
  $('.first-screen').hide();

  if (isInputMode) {
    if ($('#city-input-block').text() !== '') {
      currentCity.name = $('#city-input-block').text();
    }
  }

  var selectedCity = currentCity.name;

  $('meta[property="og:title"]').attr('content', 'Я проголосовал за город «' + selectedCity + '»');
  console.log(selectedCity);

  if (selectedCity !== '') {
    var sid = currentUser.mode + currentUser.id;

    var dataString = {
      'city': selectedCity,
      'socialID': sid
    };
    $.ajax({
      type: "POST",
      url: "vote.php",
      data: dataString,
      success: function (data) {
        console.log(data);
        if (data.code == 288) {
          $('.revote-screen').show();
          $('.footer-line').css('background-image', 'url()');

          if (currentUser.mode === 'site') {
            $('.copy-paste-block').show();
            $('.done-banner').css({
              'background-image': 'url(../img/done-back-brand.png)',
              'background-repeat': 'no-repeat'
            });
          }
        }
        if (data.code == 280) {
          $('.done-screen').show();
          $('.footer-line').css('background-image', 'url()');

          if (currentUser.mode === 'site') {
            $('.copy-paste-block').show();
            $('.done-banner').css({
              'background-image': 'url(../img/done-back-brand.png)',
              'background-repeat': 'no-repeat'
            });
          }

          $('.done-city-name').text(currentCity.name);
        }
        if (data.code == 480) {
          $('#revote-screen2').show();
          $('.footer-line').css('background-image', 'url()');

          if (currentUser.mode === 'site') {
            $('.copy-paste-block').show();
            $('.done-banner').css({
              'background-image': 'url(../img/done-back-brand.png)',
              'background-repeat': 'no-repeat'
            });
          }
        }

        resizeAppWindow(624, 1560);
        FAPI.UI.setWindowSize(624, 1560);
      }
    });

  } else {
    //$('#system-message').html('<span class="yellow">Выберите город</span>');
  }
}

function reVoteForCity() {
  var sid = currentUser.mode + currentUser.id;

  var dataString = {
    'city': currentCity.name,
    'socialID': sid
  };
  $.ajax({
    type: "POST",
    url: "unvote.php",
    data: dataString,
    success: function (data) {
      console.log(data);
      if (data.code == 288) {
        // $('#system-message').html('<span class="yellow">Нечего отменять</span>');
      }
      if (data.code == 280) {
        $('.first-screen').show();
        $('.done-screen').hide();
        $('.revote-screen').hide();

        //resizeAppWindow(624, 1930);

        refreshTop();
      }
    }
  });
}

function resizeAppWindow(width, height) {
  if (currentUser.mode === 'vk') {
    VK.callMethod('resizeWindow', width, height);
  }
}

$('.revote-btn').click(function () {
  //reVoteForCity();
  location.reload();
});

$('.revote-btn2').click(function () {
  location.reload();
});

$('.menu-button-0').click(function () {
  location.reload();

  resizeAppWindow(624, 3870);
  FAPI.UI.setWindowSize(624, 3870);

  refreshTop();
});

$('.menu-button-1').click(function () {
  $('.first-screen').show();
  $('.done-screen').hide();
  $('.revote-screen').hide();

  //resizeAppWindow(624, 1930);

  refreshTop();
});

$('#vote-btn-top').click(function () {
  console.info(currentCity.name);
  //if (currentCity.name !== '') {
  //    canVote = true;
  //}
  if ($('#city-input-block').html() != '') {
    if (currentCity.name != '') {
      voteForCity();
    } else {
      currentCity.name = $('#city-input-block').text();
      voteForCity();
    }
  } else {
    $('.city-list li').show();
    $('#scroll-bar').show();
    isInputMode = false;
    canVote = false;
    $('#vote-btn-top').css('opacity', '0.6');
    $('#minmax-btn').css('background-image', 'url(../img/icon-min2.png)');
    $('#city-input-block').html('<span>ВЫБЕРИТЕ ГОРОД</span>');
    $('#vote-btn-top').text('Я ГОЛОСУЮ!');
  }
});

$('#button-vote-block').click(function () {
  $('.content-vote').show();
  $('.content-map').hide();
});

$('#button-map-block').click(function () {
  $('.content-vote').hide();
  $('.content-map').show();

  initMap();
});

function setCurrentUser(id, city, fname, lname, mode) {
  currentUser.id = id;
  currentUser.city = city;
  currentUser.fname = fname;
  currentUser.lname = lname;
  currentUser.isFilled = true;
  currentUser.mode = mode;
}

function setCurrentCityList() {
  var cityListTop = $('.city-list').position().top;
  var cityListItems = $('.city-list li');

  var currItemNum = Math.abs((cityListTop - 120) / 32);

  currentCity.name = $(cityListItems[currItemNum]).text();

  $('#city-input-block').html(currentCity.name);
  $('#vote-btn-top').css('opacity', '1');

  canVote = true;

  console.log(currentCity.name);
  setScrollbarPos();
}

function moveListToCurrentCity(item) {
  //console.log(item.index());
  var newHeight = 120 - (item.index() * 32);

  $('.city-list').css({
    'top': newHeight + 'px'
  });

  setCurrentCityList();
}

function writeUserInfo(id, city, fname, lname, bdate, social) {
  var socialType = social || '';
  var socialUrl = '';

  if (social === 'vk') {
    socialUrl = 'https://vk.com/id' + id;
  }

  if (social === 'ok') {
    socialUrl = 'https://ok.ru/profile/' + id;
  }

  //TODO сделать корректный паресер дат рождения
  var bDate = new Date(bdate);

  var dataString = {
    'id': id,
    'city': city,
    'fname': fname,
    'lname': lname,
    'bdate': bDate.toMysqlFormat(),
    'socialtype': socialType,
    'socialurl': socialUrl
  };

  //console.info(dataString);

  $.ajax({
    type: 'POST',
    url: 'user.php',
    data: dataString,
    success: function (data) {
      //console.log(data);
      if (data.code == 288) {
        console.log('Пользователь уже есть в базе');
      }
      if (data.code == 280) {
        console.log('Пользователь добавлен');
      }
    }
  });
}

function showBrandVersion() {
  $('#banner-notbrand').hide();
  $('#banner-brand').show();
  $('.banner-text').hide();
  $('.banner-text-brand').show();
  $('#footer-copy').show();

  $('.rules-text-nobrand').hide();
  $('.rules-text').show();
}

function runSite() {
  console.info('Site mode');
  currentUser.mode = 'site';
  setCurrentUser('none', 'nowhere', 'voter', 'voter', 'site');

  showBrandVersion();
}

function runVK() {
  console.info('VK mode');

  $('.top-menu').hide();
  $('#hl1').hide();

  showBrandVersion();

  $('.footer-logo1').hide();
  $('.footer-logo2').hide();
  $('.social-ok').hide();
  $('.footer-btn1').hide();

  $('.footer-big-ok').hide();
  $('.footer-big-fb').hide();
  $('.footer-social-circle-fb').hide();

  $('.social-vk').css('left', '80px');
  $('.footer-btn2').css('right', '190px');

  $('.footer-social-circle-text').html('<span class="gb">Сообщество</span> HASBRO:</div>');

  //$('.footer-big-vk').css('left', '322px');
  //$('.footer-social-circle-vk').css('left', '192px');
  $('.footer-social-circle').css('left', '218px');


  VK.init(function () {
    console.info('inited');

    VK.api('users.get', {
      'fields': 'sex, bdate, city, country, photo_50, photo_100,verified'
    }, function (data) {
      if (data.response) {
        var r = data.response[0];

        setCurrentUser(r.id, r.city.title, r.first_name, r.last_name, 'vk');
        writeUserInfo(r.id, r.city.title, r.first_name, r.last_name, r.bdate, 'vk');
      }
    });
  }, function () {
    console.info('init failed');
  }, '5.44');
}

function runOK() {
  console.info('OK mode');

  $('.top-menu').hide();
  $('#hl1').hide();

  //FAPI.UI.setWindowSize(624,3870);

  showBrandVersion();
  $('.footer-social-circle-fb').hide();
  $('.footer-big-fb').hide();
  $('.footer-big-ok').css('left', '328px');
  $('.text-common').hide();
  $('.text-ok1').show();

  $('.menu-button-1').on('click', function () {
    window.parent.$("body").animate({scrollTop: 300}, 'slow');
    console.log('sdfsdf')
  });

  var rParams = FAPI.Util.getRequestParameters();
  FAPI.init(rParams["api_server"], rParams["apiconnection"],
    function () {
      console.info("Инициализация прошла успешно");

      FAPI.Client.call({
        'method': 'users.getCurrentUser',
        'fields': 'uid, first_name, last_name, name, gender, age, birthday, photo_id, pic_1, pic_2, location'
      }, function (method, result, data) {
        if (result) {
          console.info(result);
          var r = result;

          setCurrentUser(r.uid, r.location.city, r.first_name, r.last_name, 'ok');
          writeUserInfo(r.uid, r.location.city, r.first_name, r.last_name, r.birthday, 'ok');
        } else {
          console.log(data);
        }
      });
    },
    function (error) {
      console.info('Ошибка инициализации', error);
    }
  );
}

function cityArray(list) {
  var cityArray = [];

  $.each(list, function (index, value) {
    cityArray.push($(value).text());
  });

  return cityArray;
}

function getCities() {
  $.ajax({
    type: 'POST',
    url: 'getcities.php',
    data: {},
    success: function (data) {
      var buffer = '';

      $.each(data, function (item, value) {
        buffer += '<li class="city">' + value.name + '</li>';
      });

      $('ul.city-list').html(buffer);


      //list controller
      var cityListItems = $('.city-list li');
      var itemHeight = cityListItems.height();

      var cityInput = $('.city-input');
      var inputHeight = cityInput.height();
      var inputTop = cityInput.position().top;

      var cityList = $('.city-list');
      var cityListLength = cityListItems.length;

      currentCity.allNames = cityArray($('.city-list li'));

      $('#city-input-block').autocomplete({
        source: currentCity.allNames
      });

      //$('.city-list').mCustomScrollbar();

      $('#city-input-block').on('autocompletesearch', function (e, ui) {
        //console.log(e, ui);
        $('.ui-menu-item').on('remove', function () {
          // $(this).slideUp();
          //console.log('======')
        })
      });

      //console.log(currentCity.allNames);
      //console.log(cityListLength);

      function nextItem() {
        var cityListTop = cityList.position().top;

        if (cityListTop <= (inputTop - itemHeight * (cityListLength - 1))) {
          return;
        }

        cityList.css({
          'top': cityListTop - itemHeight + 'px'
        });

        setCurrentCityList();
      }

      function prevItem() {
        var cityListTop = cityList.position().top;

        if (cityListTop >= inputTop) {
          return;
        }

        cityList.css({
          'top': cityListTop + itemHeight + 'px'
        });

        setCurrentCityList();
      }


      $(window).keydown(function (e) {
        if (e.which === 40) {
          e.preventDefault();
          //console.log('down');
          nextItem();
        } else if (e.which === 38) {
          e.preventDefault();
          //console.log('up');
          prevItem();
        }
      });

      var isScroll = false;

      var mousewheelEvent = (/Firefox/i.test(navigator.userAgent)) ? 'DOMMouseScroll' : 'mousewheel';

      $('.list-container').bind(mousewheelEvent, function (e) {

        e.preventDefault();

        var event = window.event || e;
        event = event.originalEvent ? event.originalEvent : event;
        var delta = event.detail ? event.detail * (-40) : event.wheelDelta;

        var direction = '';
        if (delta > 0) {
          //console.log(delta);
          direction = 'up';
        } else {
          //console.log(delta);
          direction = 'down';
        }

        if (direction === 'up' && !isScroll) {
          isScroll = true;
          prevItem();
        }
        if (direction === 'down' && !isScroll) {
          isScroll = true;
          nextItem();
        }

        clearTimeout($.data(this, 'scrollTimer'));
        $.data(this, 'scrollTimer', setTimeout(function () {
          // do something
          isScroll = false;
          //console.log("Haven't scrolled in 20ms!");
        }, 30));
      });

      $('.city').on('click', function () {
        //console.log($(this).text());

        moveListToCurrentCity($(this));
      });

      //setCurrentCityList();
      setScrollbarPos();
    }
  });
}

function getVotes(limit) {
  var dataString = {
    'toplimit': limit
  };

  $.ajax({
    type: 'POST',
    url: 'getvotes.php',
    data: dataString,
    success: function (data) {
      //console.log(data);
      var buffer = '';

      $.each(data, function (item, value) {
        buffer += '<li class="top"><span class="votes-name">' + value.name + '</span> - <span class="votes-count">' + value.count + '</span></li>';
      });

      $('.city-top' + limit + ' ol').html(buffer);

      $('.city-top' + limit + ' .top').on('click', function () {
        console.log($(this).find('.votes-name').text());

        var container = $(document.getElementById('form_with_map'));

        var cityInput = container.find('[name="city"]');

        var selectedCity = $(this).find('.votes-name').text();

        cityInput.val(selectedCity);

        currentCity.name = selectedCity;
      });
    }
  });
}

function getRating(limit) {
  var dataString = {
    'toplimit': limit
  };

  $.ajax({
    type: 'POST',
    url: 'gettotalvotes.php',
    data: {},
    success: function (data) {
      var totalVotes = data[0].count;

      $.ajax({
        type: 'POST',
        url: 'getvotes.php',
        data: dataString,
        success: function (data) {
          var buffer = '';

          $.each(data, function (item, value) {
            var currIndex = item + 1;
            var percents = Math.round(parseFloat(value.count / totalVotes * 100) * 100) / 100;
            var stripeWidth = Math.round(240 * percents / 100);

            //console.log(percents, stripeWidth);

            var numbersLeft = (percents < 10) ? 38 : stripeWidth + 8;

            if (percents < 40) {
              buffer += '<li><div class="top20-city">' + value.name + '</div><div class="top20-info"><div class="top-stripe listbg' + currIndex + '" style="width:' + stripeWidth + 'px;"><div class="top20-numbers" style="left:' + numbersLeft + 'px;"><span class="top-percent">' + percents + '%</span> / <span class="top-total">' + value.count + '</span><span class="plus-one"> +1</span></div></div></div><div class="vote-btn">Голосовать</div></li>';
            } else {
              buffer += '<li><div class="top20-city">' + value.name + '</div><div class="top20-info"><div class="top-stripe listbg' + currIndex + '" style="width:' + stripeWidth + 'px;"><div class="top20-numbers"><span class="top-percent">' + percents + '%</span> / <span class="top-total">' + value.count + '</span><span class="plus-one"> +1</span></div></div></div><div class="vote-btn">Голосовать</div></li>';
            }
          });

          $('ul.top20-list').html(buffer);

          $('.vote-btn').click(function () {
            console.log($($(this).siblings()[0]).text());
            currentCity.name = $($(this).siblings()[0]).text();

            voteForCity();
          });
        }
      });
    }
  });
}

function refreshTop() {
  getVotes(10);
  getVotes(20);

  getRating(20);
}

//top tabs
$('#top20-tab').on('click', function () {
  $('.top20-content').show();
  $('.map-content').hide();
});

$('#map-tab').on('click', function () {
  $('.top20-content').hide();
  $('.map-content').show();

  initMap();
});


function copyToClipboardMsg(elem, msgElem) {
  var succeed = copyToClipboard(elem);
  var msg;
  if (!succeed) {
    msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
  } else {
    msg = "Text copied to the clipboard."
  }
  //if (typeof msgElem === "string") {
  //    msgElem = document.getElementById(msgElem);
  //}
  ////msgElem.innerHTML = msg;
  //setTimeout(function () {
  //    msgElem.innerHTML = "";
  //}, 2000);
}

function copyToClipboard(elem) {
  // create hidden text element, if it doesn't already exist
  var targetId = "_hiddenCopyText_";
  var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
  var origSelectionStart, origSelectionEnd;
  if (isInput) {
    // can just use the original source element for the selection and copy
    target = elem;
    origSelectionStart = elem.selectionStart;
    origSelectionEnd = elem.selectionEnd;
  } else {
    // must use a temporary form element for the selection and copy
    target = document.getElementById(targetId);
    if (!target) {
      var target = document.createElement("textarea");
      target.style.position = "absolute";
      target.style.left = "-9999px";
      target.style.top = "0";
      target.id = targetId;
      document.body.appendChild(target);
    }
    target.textContent = elem.textContent;
  }
  // select the content
  var currentFocus = document.activeElement;
  target.focus();
  target.setSelectionRange(0, target.value.length);

  // copy the selection
  var succeed;
  try {
    succeed = document.execCommand("copy");
  } catch (e) {
    succeed = false;
  }
  // restore original focus
  if (currentFocus && typeof currentFocus.focus === "function") {
    currentFocus.focus();
  }

  if (isInput) {
    // restore prior selection
    elem.setSelectionRange(origSelectionStart, origSelectionEnd);
  } else {
    // clear temporary content
    target.textContent = "";
  }
  return succeed;
}

$('#copy-button').on('click', function () {
  // copyToClipboard($('#copy-url'));
  copyToClipboardMsg(document.getElementById("copy-url"));
});

$('#city-input-block').on('click', function () {
  $('.city-list li').hide();
  $('#scroll-bar').hide();
  $(this).html('');
  isInputMode = true;
  $('#minmax-btn').css('background-image', 'url(../img/icon-min1.png)');

  $('#vote-btn-top').text('ОТМЕНА');
});

$('#city-input-block').on('keyup', function () {
  if ($('#city-input-block').text() != '') {
    $('#vote-btn-top').text('Я ГОЛОСУЮ!');
  } else {
    $('#vote-btn-top').text('ОТМЕНА');
  }
});

$('#city-input-block').on('focus', function () {
  $('#vote-btn-top').css('opacity', '1');
});

$('#city-input-block').on('focusout', function () {
  //$('.city-list li').show();
  //$(this).text('');
});

$('.list-container').on('click', function () {
  //$('.city-list li').show();
  //$(this).text('');
});

$('[contenteditable]').keypress(function (e) {
  if (e.keyCode == 10 || e.keyCode == 13) {
    e.preventDefault();
  }
});

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

$('#random-btn').on('click', function () {
  $('.city-list li').hide();
  $('#vote-btn-top').css('opacity', '1');
  canVote = true;
  isInputMode = true;

  var cityArrLength = currentCity.allNames.length;

  $('#city-input-block').html(currentCity.allNames[getRandomInt(0, cityArrLength)]);

  $('#minmax-btn').css('background-image', 'url(../img/icon-min1.png)');
});

$('#minmax-btn').on('click', function () {
  if (isInputMode) {
    $('.city-list li').show();
    isInputMode = false;
    canVote = false;
    $('#vote-btn-top').css('opacity', '0.6');
    $(this).css('background-image', 'url(../img/icon-min2.png)');
    $('#city-input-block').html('<span>ВЫБЕРИТЕ ГОРОД</span>');
  }
});