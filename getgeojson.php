<?php
error_reporting(0);
include('dbconf.php');

$postError = 'error';
$postSuccess = 'success';

function normJsonStr($str)
{
    $str = preg_replace_callback('/\\\u([a-f0-9]{4})/i', create_function('$m', 'return chr(hexdec($m[1])-1072+224);'), $str);
    return iconv('cp1251', 'utf-8', $str);
}

function postResponse($status, $code)
{
    header("Content-type: application/json; charset=utf-8");
    $returnData = array('status' => $status, 'code' => $code);
    echo json_encode($returnData);
}

$link = mysqli_connect($serverName, $userName, $password, $dbName);


if ($link === false) {
    postResponse($GLOBALS['postError'], '588');
    die();
}

$resultQuery = "CALL getGeo()";


mysqli_query($link, "set_client='utf8'");
mysqli_query($link, "set character_set_results='utf8'");
mysqli_query($link, "set collation_connection='utf8_general_ci'");
mysqli_query($link, "SET NAMES utf8");

$resultQueryResult = mysqli_query($link, $resultQuery);

$coords = array();

while ($row = mysqli_fetch_assoc($resultQueryResult)) {

    $feature = array('long' => $row['longitude'], 'lat' => $row['latitude']);
    array_push($coords, $feature);
}

mysqli_free_result($resultQueryResult);

header("Content-type: application/json; charset=utf-8");
echo json_encode($coords, JSON_UNESCAPED_UNICODE);

mysqli_close($link);
