DELIMITER |
CREATE DEFINER=`monopoly`@`localhost` PROCEDURE `getGeo`()
    NO SQL
SELECT name, latitude, longitude FROM votes, cities, coords WHERE votes.city=cities.id AND cities.id=coords.place_id AND cities.skip=0 AND votes.canceled=0 ORDER BY name ASC
|

DELIMITER |
CREATE DEFINER=`monopoly`@`localhost` PROCEDURE `getTop`()
    NO SQL
SELECT name, COUNT(*) AS count, latitude, longitude FROM votes, cities, coords WHERE votes.city=cities.id AND cities.id=coords.place_id AND cities.skip=0 AND votes.canceled=0 GROUP BY city ORDER BY count DESC, name ASC
|

DELIMITER |
CREATE DEFINER=`monopoly`@`localhost` PROCEDURE `getTop20`()
    NO SQL
SELECT name, COUNT(*) as count, latitude, longitude FROM votes, cities, coords WHERE votes.city=cities.id AND cities.id=coords.place_id AND cities.skip=0 AND votes.canceled=0 GROUP BY city ORDER BY count DESC, name ASC LIMIT 20
|



GRANT EXECUTE ON PROCEDURE myDB.spName TO 'TestUser'@'localhost';