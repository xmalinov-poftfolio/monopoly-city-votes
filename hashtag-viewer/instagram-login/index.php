<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>How to login with Instagram in PHP | PGPGang.com</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <style type="text/css">
        img {
            border-width: 0
        }

        * {
            font-family: 'Lucida Grande', sans-serif;
        }
    </style>
    <style>
        a.button {
            background: url(instagram-login.png) no-repeat transparent;
            cursor: pointer;
            display: block;
            height: 29px;
            margin: 50px auto;
            overflow: hidden;
            text-indent: -9999px;
            width: 200px;
        }

    </style>
</head>
<body onload="changePagination('0','first')">
<div>
    <h2>How to login with Instagram in PHP example.&nbsp;&nbsp;&nbsp;=> <a href="http://www.phpgang.com/">Home</a> | <a
            href="http://demo.phpgang.com/">More Demos</a></h2>
    <?php
    require 'instagram.class.php';
    require 'config.php';
    $instagram = new Instagram(array(
        'apiKey' => YOUR_APP_KEY,
        'apiSecret' => YOUR_APP_SECRET,
        'apiCallback' => YOUR_APP_CALLBACK
    ));


    $code = $_GET['code'];
    if (true === isset($code)) {
        $data = $instagram->getOAuthToken($code);
        print_r($data);

    } else {
        $loginUrl = $instagram->getLoginUrl();
        echo "<a class=\"button\" href=\"$loginUrl\">Sign in with Instagram</a>";
    }

    ?>
</body>
</html>