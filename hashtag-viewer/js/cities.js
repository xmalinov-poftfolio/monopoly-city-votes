var cities = [
    {
        "name": "абакан",
        "instagram": "0",
        "vk": "1"
    },
    {
        "name": "анадырь",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "астрахань",
        "instagram": "0",
        "vk": "5"
    },
    {
        "name": "барнаул",
        "instagram": "1",
        "vk": "3"
    },
    {
        "name": "биробиджан",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "благовещенск",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "брянск",
        "instagram": "1",
        "vk": "5"
    },
    {
        "name": "великийновгород",
        "instagram": "0",
        "vk": "3"
    },
    {
        "name": "владикавказ",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "владимир",
        "instagram": "0",
        "vk": "1"
    },
    {
        "name": "вологда",
        "instagram": "1",
        "vk": "0"
    },
    {
        "name": "воронеж",
        "instagram": "1",
        "vk": "9"
    },
    {
        "name": "горноалтайск",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "грозный",
        "instagram": "1",
        "vk": "1"
    },
    {
        "name": "иваново",
        "instagram": "0",
        "vk": "1"
    },
    {
        "name": "ижевск",
        "instagram": "0",
        "vk": "5"
    },
    {
        "name": "иркутск",
        "instagram": "2",
        "vk": "4"
    },
    {
        "name": "йошкарола",
        "instagram": "0",
        "vk": "25"
    },
    {
        "name": "калининград",
        "instagram": "0",
        "vk": "4"
    },
    {
        "name": "кемерово",
        "instagram": "0",
        "vk": "1"
    },
    {
        "name": "киров",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "кострома",
        "instagram": "0",
        "vk": "3"
    },
    {
        "name": "красноярск",
        "instagram": "8",
        "vk": "17"
    },
    {
        "name": "курган",
        "instagram": "0",
        "vk": "1"
    },
    {
        "name": "курск",
        "instagram": "0",
        "vk": "2"
    },
    {
        "name": "кызыл",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "липецк",
        "instagram": "2",
        "vk": "1"
    },
    {
        "name": "магадан",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "магас",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "майкоп",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "махачкала",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "мурманск",
        "instagram": "1",
        "vk": "1"
    },
    {
        "name": "нальчик",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "нарьянмар",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "орёл",
        "instagram": "0",
        "vk": "6"
    },
    {
        "name": "оренбург",
        "instagram": "0",
        "vk": "4"
    },
    {
        "name": "пенза",
        "instagram": "1",
        "vk": "0"
    },
    {
        "name": "петрозаводск",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "петропавловсккамчатский",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "псков",
        "instagram": "0",
        "vk": "1"
    },
    {
        "name": "рязань",
        "instagram": "1",
        "vk": "4"
    },
    {
        "name": "салехард",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "самара",
        "instagram": "294",
        "vk": "59"
    },
    {
        "name": "саранск",
        "instagram": "1",
        "vk": "1"
    },
    {
        "name": "саратов",
        "instagram": "215",
        "vk": "3"
    },
    {
        "name": "смоленск",
        "instagram": "0",
        "vk": "3"
    },
    {
        "name": "сыктывкар",
        "instagram": "0",
        "vk": "4"
    },
    {
        "name": "тамбов",
        "instagram": "4",
        "vk": "1"
    },
    {
        "name": "тверь",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "тула",
        "instagram": "0",
        "vk": "3"
    },
    {
        "name": "тюмень",
        "instagram": "1",
        "vk": "29"
    },
    {
        "name": "уланудэ",
        "instagram": "10",
        "vk": "6"
    },
    {
        "name": "ульяновск",
        "instagram": "0",
        "vk": "1"
    },
    {
        "name": "хантымансийск",
        "instagram": "0",
        "vk": "2"
    },
    {
        "name": "чебоксары",
        "instagram": "16",
        "vk": "1"
    },
    {
        "name": "черкесск",
        "instagram": "0",
        "vk": "2"
    },
    {
        "name": "чита",
        "instagram": "1",
        "vk": "0"
    },
    {
        "name": "элиста",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "южносахалинск",
        "instagram": "0",
        "vk": "1"
    },
    {
        "name": "якутск",
        "instagram": "1",
        "vk": "0"
    },
    {
        "name": "ярославль",
        "instagram": "0",
        "vk": "3"
    },
    {
        "name": "новосибирск",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "москва",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "санктпетербург",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "владивосток",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "екатеринбург",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "хабаровск",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "ростовнадону",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "ставрополь",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "белгород",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "волгоград",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "омск",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "нижнийновгород",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "челябинск",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "архангельск",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "краснодар",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "казань",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "уфа",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "томск",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "пермь",
        "instagram": "0",
        "vk": "0"
    },
    {
        "name": "калуга",
        "instagram": "0",
        "vk": "0"
    }
];
