<?php
error_reporting(0);
include('dbconf.php');

$postError = 'error';
$postSuccess = 'success';

//vote accepted - 280
//already voted - 288
//city adding error - 480
//vote adding error - 481
//data in post isn't received or wrong - 488
//could not connect - 588

function postResponse($status, $code)
{
    header("Content-type: application/json; charset=utf-8");
    $returnData = array('status' => $status, 'code' => $code);
    echo json_encode($returnData);
}

function voteForCity($c, $lt, $ln, $sid)
{
    $s = $GLOBALS['serverName'];
    $u = $GLOBALS['userName'];
    $p = $GLOBALS['password'];
    $d = $GLOBALS['dbName'];

    $dt = date("Y-m-d H:i:s");

    $link = mysqli_connect($s, $u, $p, $d);

    //фикс кодировки кириллицы
    mysqli_query($link, "set_client='utf8'");
    mysqli_query($link, "set character_set_results='utf8'");
    mysqli_query($link, "set collation_connection='utf8_general_ci'");
    mysqli_query($link, "SET NAMES utf8");

    if ($link === false) {
        postResponse($GLOBALS['postError'], '588'); //не соединились с базой
        die();
    }

    $unvoteCheckQuery = "SELECT * FROM `votes` WHERE socialid = '$sid' AND canceled = '0'";
    $unvoteCheckQueryResult = mysqli_query($link, $unvoteCheckQuery);

    if (mysqli_num_rows($unvoteCheckQueryResult) !== 0) {
        $unvoteQuery = "UPDATE votes SET canceled = '1' WHERE socialid = '$sid'";

        if (mysqli_query($link, $unvoteQuery) === true) {
            postResponse($GLOBALS['postSuccess'], '280');
        } else {
            postResponse($GLOBALS['postError'], '481');
        }
    } else if ($sid === 'idTEST0123456789') {
        echo('test');
    } else {
        postResponse($GLOBALS['postError'], '288');
    }

    mysqli_free_result($unvoteCheckQueryResult);

    mysqli_close($link);
}

if (isset($_POST['city']) & isset($_POST['socialID'])) {
    $city = $_POST['city'];
    $lat = $_POST['lat'];
    $lng = $_POST['lng'];
    $socialID = $_POST['socialID'];
    voteForCity($city, $lat, $lng, $socialID);
} else {
    postResponse($postError, '488'); //проблемы с параметрами post
}
