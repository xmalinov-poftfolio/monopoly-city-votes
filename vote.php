<?php
include('dbconf.php');

$postError = 'error';
$postSuccess = 'success';

//vote accepted - 280
//already voted - 288
//city adding error - 480
//vote adding error - 481
//data in post isn't received or wrong - 488
//could not connect - 588

function postResponse($status, $code)
{
    header("Content-type: application/json; charset=utf-8");
    $returnData = array('status' => $status, 'code' => $code);
    echo json_encode($returnData);
}

function voteForCity($c, $sid)
{
    $s = $GLOBALS['serverName'];
    $u = $GLOBALS['userName'];
    $p = $GLOBALS['password'];
    $d = $GLOBALS['dbName'];

    $dt = date("Y-m-d H:i:s");

    $link = mysqli_connect($s, $u, $p, $d);

    //фикс кодировки кириллицы
    mysqli_query($link, "set_client='utf8'");
    mysqli_query($link, "set character_set_results='utf8'");
    mysqli_query($link, "set collation_connection='utf8_general_ci'");
    mysqli_query($link, "SET NAMES utf8");

    if ($link === false) {
        postResponse($GLOBALS['postError'], '588'); //не соединились с базой
        die();
    }

    //проверка на наличие города в бд, если нет, то добавить город
    $checkQuery = "SELECT * FROM `cities` WHERE name = '$c'";
    $checkQueryResult = mysqli_query($link, $checkQuery);

    if (mysqli_num_rows($checkQueryResult) !== 0) {
        //проверка на уже имеющийся голос от id соцсети
        $voteCheckQuery = "SELECT * FROM `votes` WHERE socialid = '$sid' AND canceled = '0'";
        $voteCheckQueryResult = mysqli_query($link, $voteCheckQuery);

        if (mysqli_num_rows($voteCheckQueryResult) === 0) {
            //пишем в БД голос за город, для каждого соц. ID можно только 1 раз. idTEST0123456789 - тестовый айдишник
            $voteQuery = "INSERT INTO votes (city, socialid, votedate) SELECT id, '$sid', '$dt' FROM cities WHERE name = '$c'";

            if (mysqli_query($link, $voteQuery) === true) {
                postResponse($GLOBALS['postSuccess'], '280'); //голос принят
            } else {
                postResponse($GLOBALS['postError'], '481'); //проблема с добавлением голоса
            }
        } else if ($sid === 'sitenone') {
            $voteQuery = "INSERT INTO votes (city, socialid, votedate) SELECT id, '$sid', '$dt' FROM cities WHERE name = '$c'";

            if (mysqli_query($link, $voteQuery) === true) {
                postResponse($GLOBALS['postSuccess'], '280'); //голос принят
            } else {
                postResponse($GLOBALS['postError'], '481'); //проблема с добавлением голоса
            }
        } else if ($sid === 'votes-generator') {
            $voteQuery = "INSERT INTO votes (city, socialid, votedate) SELECT id, '$sid', '$dt' FROM cities WHERE name = '$c'";

            if (mysqli_query($link, $voteQuery) === true) {
                postResponse($GLOBALS['postSuccess'], '280'); //голос принят
            } else {
                postResponse($GLOBALS['postError'], '481'); //проблема с добавлением голоса
            }
        } else {
            //postResponse($GLOBALS['postError'], '288'); //уже проголосовали
            $voteQuery = "INSERT INTO votes (city, socialid, votedate) SELECT id, '$sid', '$dt' FROM cities WHERE name = '$c'";

            if (mysqli_query($link, $voteQuery) === true) {
                postResponse($GLOBALS['postSuccess'], '280'); //голос принят
            } else {
                postResponse($GLOBALS['postError'], '481'); //проблема с добавлением голоса
            }
        }
    } else {
        postResponse($GLOBALS['postError'], '480');
    }

    mysqli_free_result($checkQueryResult);
    mysqli_free_result($voteCheckQueryResult);

    mysqli_close($link);
}

if (isset($_POST['city']) & isset($_POST['socialID'])) {
    $city = $_POST['city'];
    $socialID = $_POST['socialID'];
    voteForCity($city, $socialID);
} else {
    postResponse($postError, '488'); //проблемы с параметрами post
}
